package pl.lukaszcebula;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by Łukasz on 2017-03-14.
 */


public class GuessNumber {
    /**
     * @param args
     * @throws IOException
     * @throws NumberFormatException
     */
    public static void main(String[] args) throws NumberFormatException, IOException {
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean result = false;
        int attempt = 1;
        Random rand = new Random();
        int number = rand.nextInt(100);
        while (result == false)
        {
            System.out.print("Wprowadź liczbę z zakresu 0-100: ");
            int guess = Integer.parseInt(reader.readLine());
            if(guess == number)
            {
                System.out.println("Znalazłeś szukaną liczbę!!!");
                result=true;
            }
            else if (guess>number)
            {
                System.out.println("Podałeś za wysoką liczbę. Spróbuj ponownie");
                result=false;
                attempt++;
            }
            else if (guess<number)
            {
                System.out.println("Podałeś za niską liczbę. Spróbuj ponownie");
                result=false;
                attempt++;
            }
        }
        System.out.println("Zgadłeś za " + attempt + " razem.");
    }
}